<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fpdfe extends CI_Controller {

	/**
	 * Example: FPDF 
	 *
	 * Documentation: 
	 * http://www.fpdf.org/ > Maanual
	 *
	 */
	public function index() {	
		$this->load->library('fpdf_master');

		// // Column Chart
		// $data[0] = array(470, 490, 90, 50);
		// $data[1] = array(450, 530, 110);
		// $data[2] = array(420, 580, 100);

		$data[0] = array('Men' => 910, 'Women' => 500, 'Children' => 300);
		
		// colors should never be less than columns
		$data[1] = array('#52508f','#bd2067','#0a4a18','#fa7d7d','#cf3cb1','#fa7d7d','#cf3cb1','#fa7d7d','#cf3cb1');
		
		// Column chart
		$this->fpdf->SetFont('Arial', 'BIU', 12);
		$this->fpdf->Cell(210, 5, 'Bar Graph', 0, 1, 'C');
		$this->fpdf->Ln(8);
		$valX = $this->fpdf->GetX();
		$valY = $this->fpdf->GetY();
		$this->fpdf->ColumnChart(220, 100, $data, '%l', array(255,175,100));

		$this->fpdf->Output('bar_graph.pdf','D');

	}

	public function pie() 
	{
		$this->load->library('fpdf_master');
		
		$data = array('Men' => 1110, 'Women' => 500, 'Children' => 700, 'Pets' => 100);

		// Pie chart
		$this->fpdf->SetFont('Arial', 'BIU', 12);
		$this->fpdf->Cell(0, 5, '1 - Pie chart', 0, 1);
		$this->fpdf->Ln(8);

		$this->fpdf->SetFont('Arial', '', 10);
		$valX = $this->fpdf->GetX();
		$valY = $this->fpdf->GetY();
		$this->fpdf->Cell(30, 5, 'Number of men:');
		$this->fpdf->Cell(15, 5, $data['Men'], 0, 0, 'R');
		$this->fpdf->Ln();
		$this->fpdf->Cell(30, 5, 'Number of women:');
		$this->fpdf->Cell(15, 5, $data['Women'], 0, 0, 'R');
		$this->fpdf->Ln();
		$this->fpdf->Cell(30, 5, 'Number of children:');
		$this->fpdf->Cell(15, 5, $data['Children'], 0, 0, 'R');
		$this->fpdf->Ln();
		$this->fpdf->Ln(8);

		$this->fpdf->SetXY(90, $valY);
		$col1=array(100,100,255);
		$col2=array(255,100,100);
		$col3=array(255,255,100);
		$this->fpdf->PieChart(100, 35, $data, '%l (%p)', array($col1,$col2,$col3,array(255,0,100)));
		$this->fpdf->SetXY($valX, $valY + 40);

		// Bar diagram
		$this->fpdf->SetFont('Arial', 'BIU', 12);
		$this->fpdf->Cell(0, 5, '2 - Bar diagram', 0, 1);
		$this->fpdf->Ln(8);
		$valX = $this->fpdf->GetX();
		$valY = $this->fpdf->GetY();
		$this->fpdf->BarDiagram(190, 70, $data, '%l : %v (%p)', array(255,175,100));
		$this->fpdf->SetXY($valX, $valY + 80);

		$this->fpdf->Output('pie_graph.pdf','D');
	}
}
